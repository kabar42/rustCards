#!/bin/bash -e
DOCKER_IMAGE=rust:1.20
BUILD_OPTS=$@

docker pull $DOCKER_IMAGE
docker run --rm \
    -v $(pwd):/workspace \
    $DOCKER_IMAGE \
    /bin/bash -c "cd /workspace && cargo build $BUILD_OPTS"
