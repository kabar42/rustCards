# Rust Cards

[![pipeline status](https://gitlab.com/kabar42/rustCards/badges/master/pipeline.svg)](https://gitlab.com/kabar42/rustCards/commits/master)

## Local Builds

From the root of the repo, call the build script:

`do/build.sh`

This will pull the rust docker container, if not already on the system, and then use it to build
a debug version of the project. Any options passed to the script will be forwarded to the cargo
build command. To run a release build, call:

`do/build.sh --release`

