mod card;
mod hand;
mod deck;
mod analyze_hands;

fn main() {
    let deck = deck::build_std_deck();
    let hand = hand::Hand::new(5);
    let mut hands: Vec<hand::Hand> = Vec::with_capacity(3000000);

    hand::gen_all_hands(&deck, hand, &mut hands);
    println!("Hands           : {}", hands.len());
    println!("-------------------------");

    let type_counts = analyze_hands::count_hand_types(hands);
    for (hand_type, count) in type_counts.iter().enumerate() {
        println!("{:16}: {:>7}", analyze_hands::val_to_handtype(hand_type).to_string(), *count);
    }
}
